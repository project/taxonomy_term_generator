<?php

/**
 * @file
 * Create the form for generating a terms.
 */

/**
 * Form generator.
 */
function taxonomy_term_generator_form($form, &$form_state) {

  $disabled = FALSE;
  $select_description = NULL;

  // Get all created vocabulary.
  $vocabularies = taxonomy_get_vocabularies();
  foreach ($vocabularies as $vocabulary) {
    $vocabulary_options[$vocabulary->vid] = $vocabulary->name;
  }

  // Settings if no vocabulary yet.
  if (empty($vocabulary_options)) {
    $vocabulary_options[] = array();
    $select_description = t('Please create a vocabulary first.');
    $disabled = TRUE;
  }

  $form['vocabularies_list'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary'),
    '#options' => $vocabulary_options,
    '#required' => TRUE,
    '#disabled' => $disabled,
    '#description' => $select_description,
  );

  $form['number_of_term'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of new terms'),
    '#maxlength' => 3,
    '#description' => t('Please input positive integer only. Also, you can only add up to 100.'),
    '#required' => TRUE,
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $form['name_of_term'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of terms (optional)'),
  );

  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Generate terms'),
    '#weight' => 15,
    '#disabled' => $disabled,
  );

  return($form);

}
