CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation
* Maintainers


INTRODUCTION
------------

This module will allow you to generate multiple terms on a vocabulary that 
you can use for sample or also on production.

CONFIGURATION
-------------

This module requires no configuration.
Recommend to clear Drupal cache.


INSTALLATION
-------------

1.) Install and enable this module.
2.) Go to admin/structure/taxonomy-term-generator.
3.) Select a vocabulary that you want to add some terms.
4.) Input how many terms do you want to add.
5.) Click "Generate terms".

REQUIREMENTS
------------

Core system and user module must be install

MAINTAINERS
-----------

Carlo Miguel Agno (carlagno) - https://www.drupal.org/user/3317429
